export default async function({ app: { i18n },store, redirect }) {
  if (store.state.basket.length ===0) {
    return redirect(`/${i18n.locale}/`)
  }
}


