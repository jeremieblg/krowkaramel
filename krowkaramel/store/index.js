export const state = () => ({
  products: [],
  basket:[],
  currentProduct: {},
  basketPrice: 0,
  posts:[],
  subscriptions: []
})

export const getters = {
  getProducts(state) {
    return state.products
  },
  getCurrentProduct(state) {
    return state.currentProduct
  },
  getPosts(state) {
    return state.posts
  },
  getSubscriptions(state) {
    return state.subscriptions
  },
  getBasket(state) {
    return state.basket
  },
  isAuthenticated(state) {
    return state.auth.loggedIn
  },
  loggedInUser(state) {
    return state.auth.user
  }
}

export const mutations = {
  setProducts(state, products){
    state.products = products
  },
  setPosts(state, posts){
    state.posts = posts
  },
  setSubscriptions(state, subscriptions){
    state.subscriptions = subscriptions
  },
  pushBasket(state, product){
    state.basket.push(product)
    const num = state.basketPrice + product.attributes.product_price
    state.basketPrice = Math.floor(num*100)/100;
  },
  incrementBasket(state, id){
    const found = state.basket.find(element=> element.id ===id)
    found.quantity++
    const num = state.basketPrice + found.attributes.product_price
    state.basketPrice = Math.floor(num*100)/100;
  },
  decrementBasket(state, id){
    const found = state.basket.find(element=> element.id ===id)
    found.quantity--
    const num = state.basketPrice - found.attributes.product_price
    state.basketPrice = Math.floor(num*100)/100;
  },
  removeBasket(state, id){
    const index = state.basket.map(object => object.id).indexOf(id)
    const num =state.basketPrice- (state.basket[index].attributes.product_price * state.basket[index].quantity)
    state.basketPrice = Math.floor(num*100)/100;
    state.basket.splice(index,1);
  },
  setCurrentProduct(state, id){
    state.currentProduct = state.products.find(element => element.id === id)
  }
}

export const actions = {
  async fectProducts ({ commit }) {
    const products = await this.$axios.$get('/products')
    commit('setProducts', products.data)
  },
  async fectPosts ({ commit }) {
    const posts = await this.$axios.$get('/posts')
    commit('setPosts', posts.data)
  },
  async fectSubscriptions ({ commit }) {
    const subscriptions = await this.$axios.$get('/subscriptions')
    commit('setSubscriptions', subscriptions.data)
  },
  async updateProductStockById ({ commit },id, data, quantity) {
    const stock = data.attibutes.product_stock -quantity
    const products = await this.$axios.$put(`/products/${id}`,{
      "data": {
        "product_stock": stock,
      }
    })
  },
  addToBasket ({ commit,state }, product) {
    if(state.basket.length > 0){
      if(state.basket.find(element=> element.id ===product.id)){
        commit('incrementBasket', product.id)
      }else{
        commit('pushBasket', product);
        commit('addPrice', product.product_price);
      }
    }else{
      commit('pushBasket', product);
      commit('addPrice', product.product_price);
    }
  },
  decrementFromBasket({ commit }, product){
    if(product.quantity === 1){
      commit('removeBasket', product.id);
    }else{
      commit('decrementBasket', product.id);
    }
  },
  incrementToBasket({ commit }, product){
    commit('incrementBasket', product.id)
  },
  removeFromBasket({ commit }, product){
    commit('removeBasket', product.id)
  },
  setCurrentProductById({ commit }, id){
    commit('setCurrentProduct', id)
  },
  clearAllBasketProduct({state}){
    state.basket= []
  }
}
