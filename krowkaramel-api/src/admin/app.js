export default {
  config: {
    locales: ['fr', 'en'],
	theme: {
		colors: {
		  primary100: '#87CC3D',
		  primary200: '#87CC3D',
		  primary500: '#87CC3D',
		  primary600: '#87CC3D',
		  primary700: '#87CC3D',
		  danger700: '#87CC3D',
		},
	  },
  },
  bootstrap(app) {
    console.log(app);
  },
};
