'use strict';

/**
 * product-ordrer service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::product-ordrer.product-ordrer');
