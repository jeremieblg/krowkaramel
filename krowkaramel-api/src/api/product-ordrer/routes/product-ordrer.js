'use strict';

/**
 * product-ordrer router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::product-ordrer.product-ordrer');
