'use strict';

/**
 *  product-ordrer controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::product-ordrer.product-ordrer');
