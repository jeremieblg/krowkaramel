'use strict';

/**
 * post-img router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::post-img.post-img');
