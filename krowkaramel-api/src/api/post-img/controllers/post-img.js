'use strict';

/**
 *  post-img controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::post-img.post-img');
