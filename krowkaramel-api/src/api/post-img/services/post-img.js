'use strict';

/**
 * post-img service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::post-img.post-img');
