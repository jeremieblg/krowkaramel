'use strict';

/**
 * product-subscription router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::product-subscription.product-subscription');
