'use strict';

/**
 * product-subscription service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::product-subscription.product-subscription');
