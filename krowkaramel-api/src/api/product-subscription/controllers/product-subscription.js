'use strict';

/**
 *  product-subscription controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::product-subscription.product-subscription');
