'use strict';

/**
 *  product-img controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::product-img.product-img');
