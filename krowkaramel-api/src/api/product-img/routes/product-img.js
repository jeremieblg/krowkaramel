'use strict';

/**
 * product-img router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::product-img.product-img');
