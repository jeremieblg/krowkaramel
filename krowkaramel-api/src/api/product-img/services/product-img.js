'use strict';

/**
 * product-img service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::product-img.product-img');
