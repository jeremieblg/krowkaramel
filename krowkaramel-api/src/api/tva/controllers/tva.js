'use strict';

/**
 *  tva controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::tva.tva');
