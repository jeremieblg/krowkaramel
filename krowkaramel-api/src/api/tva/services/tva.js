'use strict';

/**
 * tva service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::tva.tva');
