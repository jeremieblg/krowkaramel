'use strict';

/**
 *  company-label controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::company-label.company-label');
