'use strict';

/**
 * company-label service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::company-label.company-label');
