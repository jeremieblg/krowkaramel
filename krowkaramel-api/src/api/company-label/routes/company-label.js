'use strict';

/**
 * company-label router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::company-label.company-label');
