'use strict';

/**
 * delivery-tax service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::delivery-tax.delivery-tax');
