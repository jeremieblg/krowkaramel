'use strict';

/**
 *  delivery-tax controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::delivery-tax.delivery-tax');
