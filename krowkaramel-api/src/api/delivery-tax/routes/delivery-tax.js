'use strict';

/**
 * delivery-tax router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::delivery-tax.delivery-tax');
