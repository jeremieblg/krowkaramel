# Krowkaramel

## ⚙️ Requirement

For start this projet you need **_Nodejs v14.14.0_** and **_npm 6.14.8_**

Before the first start you need install dependances in each folder

```
In the root folder:
cd krowkaramel
npm i
```

```
In the root folder:
cd krowkaramel-api
npm i
```

## 🚀 Start

Start your application with autoReload enabled.
In the root folder

```
In the root folder
npm run start

```

# Credentials

## Admin Panel

Super Admin

```
admin@admin.fr
Admin123
```

Editor

```
editor@editor.Fr
Editor123
```

## Application

```
test@test.Fr
Test123
```

# Cupon reduction

add this in reduction input in the order page

```
A035asX5
```
